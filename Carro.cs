using System;

namespace PrimerEjercicioCarro
{
   public class Carro
    {
       public string Marca {get; set; }
       public string Modelo {get; set; }
       public int Age{get; set;}
       public string Color {get; set;}
       public string Tipo {get; set;}

       public string caracteristicas(){
           return "Marca: "+Marca+", Modelo: "+Modelo+", Año: "+Age+", Color: "+Color+", Tipo: "+Tipo;
       }

       public string frenado(){
           return "El carro va frenando";
       }

       public string acelerar(){
           return "El carro va acelerando ";
       }
       
    }
}
